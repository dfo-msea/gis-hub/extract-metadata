import xml.etree.ElementTree as ET
import os
import re
import copy
import time
import logging
import ckanapi as ck
import json
from googletrans import Translator
translator = Translator()

ET.register_namespace('gmd', "http://www.isotc211.org/2005/gmd")
ET.register_namespace('srv', "http://www.isotc211.org/2005/srv")
ET.register_namespace('gco', "http://www.isotc211.org/2005/gco")
ET.register_namespace('gmx', "http://www.isotc211.org/2005/gmx")
ET.register_namespace('xlink', "http://www.w3.org/1999/xlink")
ET.register_namespace('gmi', "http://www.isotc211.org/2005/gmi")
ET.register_namespace('gsr', "http://www.isotc211.org/2005/gsr")
ET.register_namespace('gfc', "http://www.isotc211.org/2005/gfc")
ET.register_namespace('gss', "http://www.isotc211.org/2005/gss")
ET.register_namespace('gts', "http://www.isotc211.org/2005/gts")
ET.register_namespace('gml', "http://www.opengis.net/gml/3.2")
ET.register_namespace('geonet', "http://www.fao.org/geonetwork")
ET.register_namespace('xsi', "http://www.w3.org/2001/XMLSchema-instance")


def export_meta(outdir, resource):
    """
    param outdir: output file directory
    param dataset: resource id
    """
    # try to set path to working dir
    try:
        os.chdir(outdir)
        logging.info('Working directory: %s' % outdir)

        # make out dir for json files
        meta_dir = 'metadata'
        if not os.path.exists(meta_dir):
            os.mkdir(meta_dir)
            logging.info('Output directory: %s' % os.path.join(os.getcwd(), meta_dir))

        # get metadata for resource and parent dataset
        resource_meta = ck.get_resource(resource)
        dataset_meta = ck.get_dataset(resource_meta['package_id'])

        # save json to disk
        logging.info('Exporting resource and dataset JSON metadata to %s: ' % os.path.join(os.getcwd(), meta_dir))
        json_res = os.path.join(meta_dir, '{}.json'.format(resource_meta['title']).replace(' ', '_'))
        json_dset = os.path.join(meta_dir, '{}.json'.format(dataset_meta['title']).replace(' ', '_'))
        with open(json_res, 'w') as outfile:
            json.dump(dataset_meta, outfile)

        with open(json_dset, 'w') as outfile:
            json.dump(resource_meta, outfile)

        # return path to xml dir and xml file
        return dataset_meta, resource_meta, meta_dir

    # Catching the exception
    except Exception as e:
        logging.error(e)


def get_root(xml_file):
    """
    :param xml_file: path to input xml file
    :return: root_element(xml.etree.ElementTree.Element)
    """

    # load xml template file as ElementTree
    logging.info('Loading %s as ElementTree object...' % xml_file)
    tree = ET.parse(xml_file)
    # get root tag from ElementTree
    logging.info('Getting root from ElementTree...')
    root_element = tree.getroot()
    return tree, root_element


def contacts(dset_meta, element_name):
    """
    :param dset_meta: dataset metadata
    :param element_name: 'program_manager' | 'data_creator'
    :return:  dictionary for input to HNAP xml template
    """

    # get element by name
    logging.info('Getting contact info from dataset metadata for %s...' % element_name)
    contact = json.loads(dset_meta[element_name])

    # list of keys for contact dictionary
    key_list = ['individualName', 'organisationName', 'positionName', 'voice', 'deliveryPoint', 'city',
                'administrativeArea', 'postalCode', 'country', 'electronicMailAddress', 'role']
    val_list = []
    if element_name == 'data_creator':
        contact['creator_address'] = contact['creator_address'].split(',')
        val_list = [contact['creator_name'], contact['creator_organization'], contact['creator_position'],
                    contact['creator_phone'], contact['creator_address'][0], contact['creator_address'][1],
                    contact['creator_address'][2], contact['creator_address'][3], contact['creator_address'][4],
                    contact['creator_email'], 'author']
    elif element_name == 'program_manager':
        contact['manager_address'] = contact['manager_address'].split(',')
        val_list = [contact['manager_name'], contact['manager_organization'], contact['manager_position'],
                    contact['manager_phone'], contact['manager_address'][0], contact['manager_address'][1],
                    contact['manager_address'][2], contact['manager_address'][3], contact['manager_address'][4],
                    contact['manager_email'], 'author']

    # make dictionary from lists of keys and values
    logging.info('Creating contact dictionary...')
    contact_dict = {k: v for k, v in zip(key_list, val_list)}
    return contact_dict


def data_resource(dset_meta):
    """
    :param dset_meta: dataset metadata
    :return: dictionary with URL string for resource
    """

    # get URL to zip file
    logging.info('Creating data resource dictionary...')
    data_resource_dict = {'URL': dset_meta['resources'][0]['url']}

    # get title
    data_resource_dict.update({'title': dset_meta['title']})

    # assign protocol for data transfer option (GIS-Hub uses HTTPS)
    data_resource_dict.update({'protocol': 'HTTPS'})

    # set format string... all GIS-Hub datasets will be zipped for download and in English
    desc_text = 'Dataset;ZIP;eng'
    data_resource_dict.update({'description': desc_text})

    # get name of layer in URL: such as 'hs-qcs-sponge-mpa' from https://www.gis-hub.ca/dataset/hs-qcs-sponge-mpa
    url_string = os.path.join('https://www.gis-hub.ca/dataset', dset_meta['name']).replace("\\", "/")
    data_resource_dict.update({'dataset_url': url_string})

    return data_resource_dict


def keywords(dset_meta):
    """
    :param dset_meta: dataset metadata
    :return: dictionary with list of Government of Canada Core Subject Thesaurus keywords
    """
    logging.info('Creating Government of Canada keywords dictionary...')
    kwords = dset_meta['keywords']
    keywords_list = kwords.split(',')
    keywords_dict = {'keywords': keywords_list}
    return keywords_dict


def resource_constraints():
    """
    :return: dictionary with use limitations and constraints
    """
    logging.info('Creating resource restrictions and constraints dictionary...')
    res_dict = {
        'useLimitation': 'Open Government Licence - Canada (http://open.canada.ca/en/open-government-licence-canada)',
        'accessConstraints': 'license',
        'useConstraints': 'license'}
    return res_dict


def spatial_info(res_meta, dset_meta):
    """
    :param res_meta: resource metadata
    :param dset_meta: dataset metadata
    :return: spatial dictionary with bbox, projection info, temporal info
    """

    # bounding box
    logging.info('Creating spatial information dictionary...')
    bbox = res_meta['bbox']
    bbox_split = re.split(', |: ', bbox)
    spatial_dictionary = {'westBoundLongitude': bbox_split[3],
                          'eastBoundLongitude': bbox_split[7],
                          'southBoundLatitude': bbox_split[5],
                          'northBoundLatitude': bbox_split[1]}

    # spatial reference system
    spatial_dictionary.update({'code': res_meta['projection_code'], 'codeSpace': 'http://www.epsg-registry.org'})

    # spatial data type (use latest upload version in case it's been updated, although data type unlikely to change)
    spatial_dictionary.update({'data_type': res_meta['spatial_type']})

    # start and end dates for data
    spatial_dictionary.update({'beginPosition': dset_meta['start_date'], 'endPosition': dset_meta['end_date']})
    return spatial_dictionary


def data_identification(dset_meta):
    """
    :param dset_meta: dataset metadata
    :return: id_dict
    """

    # data dates --> published & completed
    logging.info('Creating dataset identification information dictionary...')
    id_dict = {'publication_date': dset_meta['date_published'],
               'creation_date': dset_meta['date_completed']}

    # data format
    data_format = 'ZIP'

    # update dictionary
    id_dict.update({'format_name': data_format, 'format_version': 'unknown'})

    # title
    id_dict.update({'title': dset_meta['title']})

    # abstract
    id_dict.update({'abstract': dset_meta['notes']})

    # status
    id_dict.update({'status': dset_meta['status'].lower()})

    # language
    id_dict.update({'language': 'eng; CAN'})

    # topic category
    id_dict.update({'topicCategory': dset_meta['topic_category']})

    # supplementalInformation (methods)
    id_dict.update({'methods': dset_meta['methods']})

    # update frequency
    id_dict.update({'maintenanceAndUpdateFrequency': dset_meta['update_frequency'].lower()})
    return id_dict


def insert_keyword_elements(root_elem, keyword_dictionary):
    """
    :param root_elem: the root tag from the element tree
    :param keyword_dictionary: keyword dictionary (used to determine how many keyword elements to add)
    :return:
    """

    # subset identification info node, keywords node, keywords element
    logging.info('Inserting keywords into template xml file...')
    meta_info = root_elem.find('.//{http://www.isotc211.org/2005/gmd}identificationInfo')

    # get codelistvalue
    code_value = meta_info.find('.//{http://www.isotc211.org/2005/gmd}MD_CharacterSetCode')
    code_value.attrib['codeListValue'] = 'RI_458'
    code_value.text = 'utf8; utf8'

    key_node = meta_info.find('.//{http://www.isotc211.org/2005/gmd}MD_Keywords')
    key_elem = key_node.find('.//{http://www.isotc211.org/2005/gmd}keyword')

    for i in range(len(keyword_dictionary['keywords']) - 1):
        duplicate = copy.deepcopy(key_elem)
        key_node.insert(1, duplicate)

    # get all keyword elements, then their char string elements
    keys = key_node.findall('.//{http://www.isotc211.org/2005/gmd}keyword')
    # for each keyword element, insert a keyword from dictionary
    index = 0
    for i in keys:
        char_string = i.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
        char_string.text = keyword_dictionary['keywords'][index]
        # add french
        local_string = i.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
        local_string.text = translator.translate(text=keyword_dictionary['keywords'][index], src='en',
                                                 dest='fr').text
        # sleep time to prevent HTTP Status 429 (too many requests)
        time.sleep(.5)
        index += 1


def insert_contact(root_elem, contact_type, role_type, contact_dictionary, data_resource_dictionary):
    """
    :param root_elem: the root tag from the element tree
    :param contact_type: 'contact': metadata contact (program manager)
                         'identificationInfo': data contact (data creator)
                         'distributionInfo': distributor contact (program manager)
    :param role_type: pointOfContact, originator, distributor
    :param contact_dictionary: the contact dictionary (creator or manager)
    :param data_resource_dictionary: the data resource dictionary
    :return:
    """

    # set contact name
    logging.info('Inserting contact for %s into template xml file...' % contact_type)
    cont_string = './/{http://www.isotc211.org/2005/gmd}' + contact_type
    contact_element = root_elem.find(cont_string)
    responsible_party = contact_element.find('.//{http://www.isotc211.org/2005/gmd}CI_ResponsibleParty')
    ind_name = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}individualName')
    name_string = ind_name.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    name_string.text = contact_dictionary['individualName']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in ind_name.attrib:
        del ind_name.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # set contact organization
    org_name = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}organisationName')
    org_string = org_name.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    org_string.text = contact_dictionary['organisationName']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in org_name.attrib:
        del org_name.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    # add value to attrib
    org_name.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] = 'gmd:PT_FreeText_PropertyType'

    # set online resource
    online_res = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}CI_OnlineResource')
    online_res_text = online_res.find('.//{http://www.isotc211.org/2005/gmd}URL')
    online_res_text.text = data_resource_dictionary['dataset_url']

    # add french
    free_text = ET.SubElement(org_name, '{http://www.isotc211.org/2005/gmd}PT_FreeText')
    text_group = ET.SubElement(free_text, '{http://www.isotc211.org/2005/gmd}textGroup')
    local_string = ET.SubElement(text_group, '{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.attrib = {'locale': '#fra'}
    local_string.text = contact_dictionary['organisationName']
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)

    # set contact position
    pos_name = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}positionName')
    pos_string = pos_name.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    pos_string.text = contact_dictionary['positionName']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in pos_name.attrib:
        del pos_name.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    # add value to attrib
    pos_name.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] = 'gmd:PT_FreeText_PropertyType'

    # add french
    free_text = ET.SubElement(pos_name, '{http://www.isotc211.org/2005/gmd}PT_FreeText')
    text_group = ET.SubElement(free_text, '{http://www.isotc211.org/2005/gmd}textGroup')
    local_string = ET.SubElement(text_group, '{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.attrib = {'locale': '#fra'}
    local_string.text = translator.translate(text=contact_dictionary['positionName'], src='en', dest='fr').text
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)

    # set contact phone number
    phone = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}CI_Telephone')
    voice = phone.find('.//{http://www.isotc211.org/2005/gmd}voice')
    phone_text = phone.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    phone_text.text = contact_dictionary['voice']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in voice.attrib:
        del voice.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = voice.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = contact_dictionary['voice']

    # set contact address
    address = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}deliveryPoint')
    address_text = address.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    address_text.text = contact_dictionary['deliveryPoint']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in address.attrib:
        del address.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = address.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = contact_dictionary['deliveryPoint']

    # set contact city
    city = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}city')
    city_text = city.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    city_text.text = contact_dictionary['city']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in city.attrib:
        del city.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    # add value to attrib
    city.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] = 'gmd:PT_FreeText_PropertyType'

    # add french
    free_text = ET.SubElement(city, '{http://www.isotc211.org/2005/gmd}PT_FreeText')
    text_group = ET.SubElement(free_text, '{http://www.isotc211.org/2005/gmd}textGroup')
    local_string = ET.SubElement(text_group, '{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.attrib = {'locale': '#fra'}
    local_string.text = contact_dictionary['city']

    # set contact province
    province = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}administrativeArea')
    province_text = province.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    province_text.text = contact_dictionary['administrativeArea']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in province.attrib:
        del province.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = province.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = translator.translate(text=contact_dictionary['administrativeArea'], src='en', dest='fr').text
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)

    # set contact postal code
    postal_code = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}postalCode')
    postal_code_text = postal_code.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    postal_code_text.text = contact_dictionary['postalCode']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in postal_code.attrib:
        del postal_code.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # set contact country
    country = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}country')
    country_text = country.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    country_text.text = 'Canada'
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in country.attrib:
        del country.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = country.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = 'Canada'

    # set contact email address
    email = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}electronicMailAddress')
    email_text = email.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    email_text.text = contact_dictionary['electronicMailAddress']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in email.attrib:
        del email.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = email.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = contact_dictionary['electronicMailAddress']

    # set contact role
    role = responsible_party.find('.//{http://www.isotc211.org/2005/gmd}CI_RoleCode')
    # use role to assign code value and text
    if role_type == "distributor":
        role.attrib['codeListValue'] = 'RI_412'
        role.text = 'distributor; distributeur'
    if role_type == "originator":
        role.attrib['codeListValue'] = 'RI_413'
        role.text = 'originator; auteur'
    if role_type == "pointOfContact":
        role.attrib['codeListValue'] = 'RI_414'
        role.text = 'pointOfContact; contact'


def insert_identification(root_elem, identification_dictionary):
    """
    :param root_elem: the root tag from the element tree
    :param identification_dictionary: dictionary with identification information such as title
    :return:
    """
    # get citation element
    logging.info('Inserting identification information into template xml file...')
    citation = root_elem.find('.//{http://www.isotc211.org/2005/gmd}citation')

    # set title
    title = citation.find('.//{http://www.isotc211.org/2005/gmd}title')
    title_text = title.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    title_text.text = identification_dictionary['title']

    # find all data elements in citation element
    all_dates = citation.findall('.//{http://www.isotc211.org/2005/gmd}CI_Date')

    # set publication date
    publication_date = all_dates[0]
    publication_date_text = publication_date.find('.//{http://www.isotc211.org/2005/gco}Date')
    publication_date_text.text = identification_dictionary['publication_date']

    # set creation date
    create_date = all_dates[1]
    create_date_text = create_date.find('.//{http://www.isotc211.org/2005/gco}Date')
    create_date_text.text = identification_dictionary['creation_date']

    # set abstract
    abstract = root_elem.find('.//{http://www.isotc211.org/2005/gmd}abstract')
    abstract_text = abstract.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    abstract_text.text = identification_dictionary['abstract']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in abstract.attrib:
        del abstract.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # add french
    local_string = abstract.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = translator.translate(text=identification_dictionary['abstract'], src='en', dest='fr').text
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)

    # set format version
    format_element = root_elem.find('.//{http://www.isotc211.org/2005/gmd}distributionFormat')
    format_name = format_element.find('.//{http://www.isotc211.org/2005/gmd}name')
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in format_name.attrib:
        del format_name.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    format_name_text = format_name.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    format_name_text.text = identification_dictionary['format_name']

    # set format version
    format_version = format_element.find('.//{http://www.isotc211.org/2005/gmd}version')
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in format_version.attrib:
        del format_version.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    format_version_text = format_version.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    format_version_text.text = identification_dictionary['format_version']

    # set status & value code (https://gcgeo.gc.ca/geonetwork/srv/eng/metadata.create status > XML view to see codes)
    status = root_elem.find('.//{http://www.isotc211.org/2005/gmd}MD_ProgressCode')
    status.text = identification_dictionary['status']
    if identification_dictionary['status'] == 'completed':
        status.attrib['codeListValue'] = 'RI_593'
    elif identification_dictionary['status'] == 'ongoing':
        status.attrib['codeListValue'] = 'RI_596'

    # set topic category (assumes there is only one)
    topic = root_elem.find('.//{http://www.isotc211.org/2005/gmd}MD_TopicCategoryCode')
    topic.text = identification_dictionary['topicCategory']
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in topic.attrib:
        del topic.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # set update frequency. These may need to be modified. Unsure about the hub text for certain options
    update_freq = root_elem.find('.//{http://www.isotc211.org/2005/gmd}MD_MaintenanceFrequencyCode')
    if identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'continual':
        update_freq.text = 'continual; continue'
        update_freq.attrib['codeListValue'] = 'RI_532'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'biannually':
        update_freq.text = 'biannually; semestriel'
        update_freq.attrib['codeListValue'] = 'RI_538'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'annually':
        update_freq.text = 'annually; annuel'
        update_freq.attrib['codeListValue'] = 'RI_539'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'as needed':
        update_freq.text = 'asNeeded; auBesoin'
        update_freq.attrib['codeListValue'] = 'RI_540'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'irregular':
        update_freq.text = 'irregular; irrégulier'
        update_freq.attrib['codeListValue'] = 'RI_541'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'not planned':
        update_freq.text = 'notPlanned; nonPlanifié'
        update_freq.attrib['codeListValue'] = 'RI_542'
    elif identification_dictionary['maintenanceAndUpdateFrequency'].lower() == 'unknown':
        update_freq.text = 'unknown; inconnu'
        update_freq.attrib['codeListValue'] = 'RI_543'
    else:
        print('Update and maintenance frequency value is invalid')

    # clean use limitation element
    use_limitation = root_elem.find('.//{http://www.isotc211.org/2005/gmd}useLimitation')
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in use_limitation.attrib:
        del use_limitation.attrib['{http://www.isotc211.org/2005/gco}nilReason']

    # set supplemental information
    methods = root_elem.find('.//{http://www.isotc211.org/2005/gmd}supplementalInformation')
    # If key '{http://www.isotc211.org/2005/gco}nilReason' exists in dictionary then delete it using del.
    if '{http://www.isotc211.org/2005/gco}nilReason' in methods.attrib:
        del methods.attrib['{http://www.isotc211.org/2005/gco}nilReason']
    methods.attrib['{http://www.w3.org/2001/XMLSchema-instance}type'] = 'gmd:PT_FreeText_PropertyType'
    methods_text = methods.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    methods_text.text = identification_dictionary['methods']

    # add french
    free_text_methods = ET.SubElement(methods, '{http://www.isotc211.org/2005/gmd}PT_FreeText')
    text_group_methods = ET.SubElement(free_text_methods, '{http://www.isotc211.org/2005/gmd}textGroup')
    loc_string_methods = ET.SubElement(text_group_methods, '{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    loc_string_methods.attrib = {'locale': '#fra'}
    loc_string_methods.text = translator.translate(text=identification_dictionary['methods'], src='en', dest='fr').text
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)


def insert_spatial(root_elem, spatial_dictionary):
    """
    :param root_elem: the root tag from the element tree
    :param spatial_dictionary: dictionary with bbox, projection info, temporal info
    :return:
    """

    # set reference system code and codespace
    logging.info('Inserting spatial information into template xml file...')
    ref_sys = root_elem.find('.//{http://www.isotc211.org/2005/gmd}RS_Identifier')
    ref_sys_code = ref_sys.find('.//{http://www.isotc211.org/2005/gmd}code')
    ref_sys_code_text = ref_sys_code.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    ref_sys_code_text.text = spatial_dictionary['code']
    ref_sys_codespace = ref_sys.find('.//{http://www.isotc211.org/2005/gmd}codeSpace')
    ref_sys_codespace_text = ref_sys_codespace.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    ref_sys_codespace_text.text = spatial_dictionary['codeSpace']

    # set bounding box
    west = root_elem.find('.//{http://www.isotc211.org/2005/gmd}westBoundLongitude')
    west_long = west.find('.//{http://www.isotc211.org/2005/gco}Decimal')
    west_long.text = spatial_dictionary['westBoundLongitude']
    east = root_elem.find('.//{http://www.isotc211.org/2005/gmd}eastBoundLongitude')
    east_long = east.find('.//{http://www.isotc211.org/2005/gco}Decimal')
    east_long.text = spatial_dictionary['eastBoundLongitude']
    south = root_elem.find('.//{http://www.isotc211.org/2005/gmd}southBoundLatitude')
    south_lat = south.find('.//{http://www.isotc211.org/2005/gco}Decimal')
    south_lat.text = spatial_dictionary['southBoundLatitude']
    north = root_elem.find('.//{http://www.isotc211.org/2005/gmd}northBoundLatitude')
    north_lat = north.find('.//{http://www.isotc211.org/2005/gco}Decimal')
    north_lat.text = spatial_dictionary['northBoundLatitude']

    # set begin date (temporal extent)
    begin = root_elem.find('.//{http://www.opengis.net/gml/3.2}beginPosition')
    begin.text = spatial_dictionary['beginPosition']

    # set end date (temporal extent)
    end = root_elem.find('.//{http://www.opengis.net/gml/3.2}endPosition')
    end.text = spatial_dictionary['endPosition']

    # get spatial representation type (vector or raster) TO-DO: work with text/tables/csv?
    spatial_rep = root_elem.find('.//{http://www.isotc211.org/2005/gmd}MD_SpatialRepresentationTypeCode')

    # transform data_type to NAP-compliant spatial_representation
    if spatial_dictionary['data_type'].lower() == 'raster':
        spatial_rep.attrib['codeListValue'] = 'RI_636'
        spatial_rep.text = 'grid; grille'
    elif spatial_dictionary['data_type'].lower() == 'vector':
        spatial_rep.attrib['codeListValue'] = 'RI_635'
        spatial_rep.text = 'vector; vecteur'
    else:
        print('Spatial data format is invalid or missing.')


def insert_data_resource(root_elem, data_resource_dictionary):
    """
    :param root_elem: the root tag from the element tree
    :param data_resource_dictionary: dictionary with URL, name, protocol, and description
    :return:
    """
    # transferOptions
    #     |--MD_DigitalTransferOptions
    #         |--onLine
    #             |--CI_OnlineResource
    #                 |--linkage
    #                     |--URL
    #                 |--protocol
    #                     |--CharacterString
    #                 |--name
    #                     |--CharacterString
    #                 |--description
    #                     |--CharacterString

    # get MD_DigitalTransferOptions element
    logging.info('Inserting data resource information into template xml file...')
    transfer_elem = root_elem.find('.//{http://www.isotc211.org/2005/gmd}MD_DigitalTransferOptions')

    # find URL sub-element and set value
    url_elem = transfer_elem.find('.//{http://www.isotc211.org/2005/gmd}URL')
    url_elem.text = data_resource_dictionary['URL']

    # find protocol sub-elem and charstring elem and set value
    protocol_elem = transfer_elem.find('.//{http://www.isotc211.org/2005/gmd}protocol')
    protocol_elem_text = protocol_elem.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    protocol_elem_text.text = data_resource_dictionary['protocol']

    # find name sub-elem and set value
    name_elem = transfer_elem.find('.//{http://www.isotc211.org/2005/gmd}name')
    name_elem_text = name_elem.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    name_elem_text.text = data_resource_dictionary['title']

    # add french
    free_text = name_elem.find('.//{http://www.isotc211.org/2005/gmd}PT_FreeText')
    local_string = free_text.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = translator.translate(text=data_resource_dictionary['title'], src='en', dest='fr').text
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)

    # add description sub-elem and set value
    desc_elem = transfer_elem.find('.//{http://www.isotc211.org/2005/gmd}description')
    # add value to attrib
    desc_elem_text = desc_elem.find('.//{http://www.isotc211.org/2005/gco}CharacterString')
    desc_elem_text.text = data_resource_dictionary['description']

    # add french
    free_text = desc_elem.find('.//{http://www.isotc211.org/2005/gmd}PT_FreeText')
    local_string = free_text.find('.//{http://www.isotc211.org/2005/gmd}LocalisedCharacterString')
    local_string.text = 'Données;ZIP;eng'
    # sleep time to prevent HTTP Status 429 (too many requests)
    time.sleep(.5)


def run(resource_id, outdir, nap_xml):
    """
    :param resource_id: resource id (from gis hub url such as '83d8988e-916d-43f4-88ce-0d814c771ece' from
     https://www.gis-hub.ca/dataset/env-layers-qcs-20-m/resource/83d8988e-916d-43f4-88ce-0d814c771ece)
    :param outdir: output directory. should already exist. will chdir here and create another dir
    :param nap_xml: template xml file (see:
    https://gcgeo.gc.ca/geonetwork/metadata/eng/09517ca6-7657-4259-b925-dc029f01d3c4)
    :return:
    """

    # change to outdir. get resource metadata, parent (dataset) metadata and write both to disk
    try:
        xml_info = export_meta(outdir, resource_id)
    except Exception as e:
        logging.error(e)
        return

    # get root elements from NAP template and from GIS Hub dataset metadata (xml file)
    try:
        template_root = get_root(nap_xml)
    except Exception as e:
        logging.error(e)
        return

    # create series of dictionaries by extracting pieces of GIS-Hub metadata
    # If any of these pieces are missing, we cannot continue.
    try:
        mgr_dict = contacts(xml_info[0], 'program_manager')
        creator_dict = contacts(xml_info[0], 'data_creator')
        data_resource_dict = data_resource(xml_info[0])
        keywords_dict = keywords(xml_info[0])
        constraints_dict = resource_constraints()
        spatial_dict = spatial_info(xml_info[1], xml_info[0])
        identification_dict = data_identification(xml_info[0])
    except Exception as e:
        logging.error(e)
        return

    # insert metadata from dictionaries into template xml
    try:
        # ------------------------------------------------------------#
        # insert metadata contact
        # ------------------------------------------------------------#
        insert_contact(template_root[1], 'contact', 'pointOfContact', mgr_dict, data_resource_dict)
        # ------------------------------------------------------------#
        # insert data identification contact (data creator)
        # ------------------------------------------------------------#
        insert_contact(template_root[1], 'identificationInfo', 'originator', creator_dict, data_resource_dict)
        # ------------------------------------------------------------#
        # insert data distributor contact
        # ------------------------------------------------------------#
        insert_contact(template_root[1], 'distributionInfo', 'distributor', mgr_dict, data_resource_dict)
        # ------------------------------------------------------------#
        # insert spatial (bounding box, temporal extent, reference sys)
        # ------------------------------------------------------------#
        insert_spatial(template_root[1], spatial_dict)
        # ------------------------------------------------------------#
        # insert GC Core Subject Thesaurus Keywords
        # ------------------------------------------------------------#
        insert_keyword_elements(template_root[1], keywords_dict)
        # ------------------------------------------------------------#
        # insert identificationInfo
        # ------------------------------------------------------------#
        insert_identification(template_root[1], identification_dict)
        # ------------------------------------------------------------#
        # insert data resource info
        # ------------------------------------------------------------#
        insert_data_resource(template_root[1], data_resource_dict)
    except Exception as e:
        logging.error(e)

    # write new metadata XML file
    try:
        logging.info('Writing xml file to disk...')
        template_root[0].write(os.path.join(xml_info[2], data_resource_dict['title'].replace(' ', '_') + '.xml'),
                               encoding='UTF-8',
                               xml_declaration=True)
    except Exception as e:
        logging.error(e)
