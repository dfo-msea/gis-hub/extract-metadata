# extract-metadata
Script to extract metadata from the GIS Hub, and transform it into HNAP-valid XML files for submission to FGP.

CONTACT
------------

**Cole Fields**   
**Affiliation:** Fisheries and Oceans Canada (DFO)     
**Group:** Marine Spatial Ecology and Analysis     
**Location:** Institute of Ocean Sciences     
**Contact:** e-mail: Cole.Fields@dfo-mpo.gc.ca | tel: 250.363.8060
**Alternative Contact:** email: Joanne.Lessard@dfo-mpo.gc.ca | tel: 250.729.8364


DESCRIPTION
------------
### hnap_functions.py
 - Series of functions to extract and transform resource and dataset metadata from JSON formatted GIS Hub metadata to HNAP XML format.

### hnap_export.py
 - Run from the command line with a python environment that has required packages.
 - Provide arguments -r resourceid -o outputdirectory -t templatexml.
 - Requires xml.etree, os, re, copy, time, logging, ckanapi, json, googletrans
 - Starts the run function from hnap_functions.py which executes a series of functions to create XML that will be submitted to FGP

STEPS
------------
### hnap_functions.py
* Several functions to pull relevant metadata from GIS Hub metadata and insert it into XML template
* Includes things like contact information, spatial information, keywords, abstract/summary, etc.

### hnap_export.py
* Calls functions - the extraction functions first, and then the insert functions for various XML tags
* Writes XML file to disk

CAVEATS
------------

* Requires admin CKAN API key set as an environment variable on local machine.
* This only works with python virtual env that has modules installed. 
* googletrans has been inconsistent with running properly. See https://stackoverflow.com/questions/52455774/googletrans-stopped-working-with-error-nonetype-object-has-no-attribute-group.
* Work in progress.
* Produces valid xml on FGP site on tested resources.

