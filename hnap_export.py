# import modules.
import argparse
import hnap_functions as hnap
import logging
import os


def main():
    parser = argparse.ArgumentParser('Export HNAP metadata standard xml file generated from GIS Hub dataset metadata.')
    parser.add_argument('-r', '--resourceid', type=str, default=None,
                        help='GIS Hub resource id.')
    parser.add_argument('-o', '--outdir', type=str, default=None,
                        help='Output directory to write xml file.')
    parser.add_argument('-t', '--templatexml', type=str, default=None,
                        help='HNAP template xml file.')
    args = parser.parse_args()

    # Write a text file in output directory.
    logging.basicConfig(filename=os.path.join(args.outdir, "messages-export.log"),
                        filemode="w",
                        format="%(message)s",
                        level=logging.INFO)
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logging.getLogger("").addHandler(console)

    # Run functions to get metadata and export xml file.
    hnap.run(args.resourceid, args.outdir, args.templatexml)


if __name__ == "__main__":
    main()
